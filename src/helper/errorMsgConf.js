module.exports = {
  accountNotRegistered: {
    zh: '尚未找到对应于此邮箱的帐户，请检查。',
    en: 'The corresponding account for this mailbox has not been found. Please check it.'
  },
  wrongAccountOrPwd: {
    zh: '用户名或密码错误。',
    en: 'Incorrect username or password.'
  },
  accountNoActive: {
    zh: '此账号尚未激活。',
    en: 'This account has not been activated yet'
  },
  nameHadRegistered: {
    zh: '该名称已被注册。',
    en: 'The name has been registered.'
  },
  activeValidationFailed: {
    zh: `验证失败，验证链接无效或已经过期，请重新 <a href='/login'>注册</a>。`,
    en: `Validation failed. The validation link is invalid or has expired. Please <a href='/login'>re register</a>`
  },
  tokenValidationFailed: {
    zh: 'Token 验证失败，或已过期',
    en: 'Token Validation Failed，Or expired'
  },
  mailboxHadRegistered: {
    zh: '您填写的邮箱已经注册了。',
    en: 'The mailbox you filled in has been registered.'
  },
  pleaseActiveMailbox: {
    zh: '您填写的邮箱已经注册了，请激活。',
    en: 'The mailbox you filled in has been registered，Please activate it.'
  },
  noPassword: {
    zh: '您必须输入密码。',
    en: 'You must enter a password.'
  },
  noUsername: {
    zh: '您必须输入用户名。',
    en: 'You must enter an username.'
  },
  noMailbox: {
    zh: '您必须输入电子邮件地址。',
    en: 'You must enter an email address.'
  },
  uploadAbatarFail: {
    zh: '上传图片失败',
    en: 'Uploading pictures failed.'
  }
}