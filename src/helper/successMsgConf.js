module.exports = {
  sendEmailSuccess: {
    zh: '已发送邮件至 @# 请在24小时内按照邮件提示激活。',
    en: 'An e-mail has been sent to @# please within 24 hours in accordance with the message prompts activation.'
  },
  resetPwdSuccess: {
    zh: '成功重新设置密码',
    en: 'Successfully reset the password'
  }
}